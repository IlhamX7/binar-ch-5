function pilihanComputer() {
    const com = Math.random();
    const lawan1 = document.querySelector('#musuh1');
    const lawan2 = document.querySelector('#musuh2');
    const lawan3 = document.querySelector('#musuh3');

    if( com < 0.34) {
        lawan1.classList.add('dipilih4');
        lawan2.classList.remove('dipilih5');
        lawan3.classList.remove('dipilih6');
        return 'batu2';
    } else if( com >= 0.34 && com < 0.67 ) {
        lawan2.classList.add('dipilih5');
        lawan1.classList.remove('dipilih4');
        lawan3.classList.remove('dipilih6');
        return 'kertas2';
    } else {
        lawan3.classList.add('dipilih6');
        lawan1.classList.remove('dipilih4');
        lawan2.classList.remove('dipilih5');
        return 'gunting2';
    }
}

function getResult(player, com) {
    if (player == 'kertas1 dipilih2' && com == 'batu2') {
        return 'PLAYER 1 WIN';
    } else if (player == 'kertas1 dipilih2' && com == 'kertas2') {
        return 'DRAW';
    } else if (player == 'kertas1 dipilih2' && com == 'gunting2') {
        return 'COM<br>WIN';
    } else if (player == 'gunting1 dipilih3' && com == 'batu2') {
        return 'COM<br>WIN';
    } else if (player == 'gunting1 dipilih3' && com == 'kertas2') {
        return 'PLAYER 1 WIN';
    } else if (player == 'gunting1 dipilih3' && com == 'gunting2') {
        return 'DRAW';
    } else if (player == 'batu1 dipilih1' && com == 'batu2') {
        return 'DRAW';
    } else if (player == 'batu1 dipilih1' && com == 'kertas2') {
        return 'COM<br>WIN';
    } else if (player == 'batu1 dipilih1' && com == 'gunting2') {
        return 'PLAYER 1 WIN';
    }
}

function Refresh() {
    location.reload();
}

keadaan.classList.remove('status');
const versus = document.querySelector('.versus');
versus.innerHTML = 'VS';

const pBatu1 = document.querySelector('.batu1');
pBatu1.addEventListener('click', function(e) {

    if( e.target.className == 'batu1' ) {
        e.target.classList.add('dipilih1');
        keadaan.classList.add('status');
    }
    pKertas1.classList.remove('dipilih2');
    pGunting1.classList.remove('dipilih3');

    const pilihanPlayer = pBatu1.className;
    const pilihanCom = pilihanComputer();
    const hasil = getResult(pilihanPlayer, pilihanCom);
    console.log('player : ' + pilihanPlayer);
    console.log('com : ' + pilihanCom);
    console.log('hasil : ' + hasil);

    const status = document.querySelector('.status');
    const versus = document.querySelector('.versus');
    versus.innerHTML = '';
    status.innerHTML = hasil;
});

const pKertas1 = document.querySelector('.kertas1');
pKertas1.addEventListener('click', function(e) {

    if( e.target.className == 'kertas1' ) {
        e.target.classList.add('dipilih2');
        keadaan.classList.add('status');
    }
    pBatu1.classList.remove('dipilih1');
    pGunting1.classList.remove('dipilih3');

    const pilihanPlayer = pKertas1.className;
    const pilihanCom = pilihanComputer();
    const hasil = getResult(pilihanPlayer, pilihanCom);
    console.log('player : ' + pilihanPlayer);
    console.log('com : ' + pilihanCom);
    console.log('hasil : ' + hasil);

    const status = document.querySelector('.status');
    const versus = document.querySelector('.versus');
    versus.innerHTML = '';
    status.innerHTML = hasil;
});

const pGunting1 = document.querySelector('.gunting1');
pGunting1.addEventListener('click', function(e) {

    if( e.target.className == 'gunting1' ) {
        e.target.classList.add('dipilih3');
        keadaan.classList.add('status');
    }
    pBatu1.classList.remove('dipilih1');
    pKertas1.classList.remove('dipilih2');

    const pilihanPlayer = pGunting1.className;
    const pilihanCom = pilihanComputer();
    const hasil = getResult(pilihanPlayer, pilihanCom);
    console.log('player : ' + pilihanPlayer);
    console.log('com : ' + pilihanCom);
    console.log('hasil : ' + hasil);

    const status = document.querySelector('.status');
    const versus = document.querySelector('.versus');
    versus.innerHTML = '';
    status.innerHTML = hasil;
});